﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Binddata();
    }

    private static DataSet Binddata()
    {

        String strConnString = ConfigurationManager.ConnectionStrings["CorporateStoreConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConnString);
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataSet ds = new DataSet();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "USP_GetOrdercount";
        cmd.Connection = con;
        try
        {
            con.Open();
            da.SelectCommand = cmd;
            da.Fill(ds);
            return ds;

        }

        catch (Exception ex)
        {
            throw ex;
        }

        finally
        {
            con.Close();
            con.Dispose();
        }
    }


    private static DataSet bulkBinddata()
    {

        String strConnString = ConfigurationManager.ConnectionStrings["CorporateStoreConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(strConnString);
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataSet ds = new DataSet();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "USP_GetbulkOrdercount";
        cmd.Connection = con;
        try
        {
            con.Open();
            da.SelectCommand = cmd;
            da.Fill(ds);
            return ds;

        }

        catch (Exception ex)
        {
            throw ex;
        }

        finally
        {
            con.Close();
            con.Dispose();
        }
    }

    [System.Web.Services.WebMethod]
    public static List<object> getorderdata()
    {
        List<object> iData = new List<object>();
        List<string> labels = new List<string>();
        DataSet ds = Binddata();
        foreach (DataRow drow in ds.Tables[1].Rows)
        {
            int iMonthNo =Convert.ToInt16( drow["month_number"]);
            DateTime dtDate = new DateTime(2020, iMonthNo, 1);
            string sMonthName = dtDate.ToString("MMM");
            string sMonthFullName = dtDate.ToString("MMMM");
            labels.Add(sMonthFullName);
        }
        iData.Add(labels);

        List<int> lst_dataItem_1 = new List<int>();
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            lst_dataItem_1.Add(Convert.ToInt32(dr["ordercount"].ToString()));
        }
        iData.Add(lst_dataItem_1);

        List<int> lst_dataItem_2 = new List<int>();
        foreach (DataRow dr in ds.Tables[1].Rows)
        {
            lst_dataItem_2.Add(Convert.ToInt32(dr["ordercount"].ToString()));
        }
        iData.Add(lst_dataItem_2);
        return iData;
    }

    [System.Web.Services.WebMethod]
    public static List<object> getbullkorderdata()
    {
        List<object> iData = new List<object>();
        List<string> labels = new List<string>();
        DataSet ds = bulkBinddata();
        foreach (DataRow drow in ds.Tables[1].Rows)
        {
            int iMonthNo = Convert.ToInt16(drow["month_number"]);
            DateTime dtDate = new DateTime(2020, iMonthNo, 1);
            string sMonthName = dtDate.ToString("MMM");
            string sMonthFullName = dtDate.ToString("MMMM");
            labels.Add(sMonthFullName);
        }
        iData.Add(labels);

        List<int> lst_dataItem_1 = new List<int>();
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            lst_dataItem_1.Add(Convert.ToInt32(dr["ordercount"].ToString()));
        }
        iData.Add(lst_dataItem_1);

        List<int> lst_dataItem_2 = new List<int>();
        foreach (DataRow dr in ds.Tables[1].Rows)
        {
            lst_dataItem_2.Add(Convert.ToInt32(dr["ordercount"].ToString()));
        }
        iData.Add(lst_dataItem_2);
        return iData;
    }
}