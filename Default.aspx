﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>

   
</head>
<body>
   
        
        <canvas id="chartjs-example2" height="110"></canvas>
     <canvas id="chartjs-example1" height="110"></canvas>
        
  
  
    <script>
        window.chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(231,233,237)'
        };

        var color = Chart.helpers.color;

        $(document).ready(function () {
           
            $.ajax({
                type: "POST",
                url: "http://localhost:61674/Default.aspx/getorderdata",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess_,
                error: OnErrorCall_
            });


            function OnSuccess_(reponse) {
                var aData = reponse.d;
                var aLabels = aData[0];
                var aDatasets1 = aData[1];
                var aDatasets2 = aData[2];
                var d = new Date();
                var previousyear = d.getFullYear() - 1;
                var currentyear = d.getFullYear() ;
                var barChartData = {
                    labels: aLabels,
                    datasets: [{
                        label: previousyear,
                        fill: false,
                        backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.red,
                        borderWidth: 1,
                        data: aDatasets2
                        
                    },
                    {
                        label: currentyear,
                        fill: false,
                        backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.blue,
                        borderWidth: 1,
                        data:aDatasets1
                        
                        }


                    ]

                };
             

                var ctx = document.getElementById("chartjs-example2").getContext("2d");
                window.myBar = new Chart(ctx, {
                    type: 'line',
                    data: barChartData,
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Orders'
                        }
                    }
                });



            }

            function OnErrorCall_(repo) {
                alert("Woops something went wrong, pls try later !");
            }



            $.ajax({
                type: "POST",
                url: "http://localhost:61674/Default.aspx/getbullkorderdata",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                error: OnErrorCall
            });


            function OnSuccess(reponse) {
                var aData = reponse.d;
                var aLabels = aData[0];
                var aDatasets1 = aData[1];
                var aDatasets2 = aData[2];
                var d = new Date();
                var previousyear = d.getFullYear() - 1;
                var currentyear = d.getFullYear();
                var barChartData = {
                    labels: aLabels,
                    datasets: [{
                        label: previousyear,
                        fill: false,
                        backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.red,
                        borderWidth: 1,
                        data: aDatasets2

                    },
                    {
                        label: currentyear,
                        fill: false,
                        backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.blue,
                        borderWidth: 1,
                        data: aDatasets1

                    }


                    ]

                };


                var ctx = document.getElementById("chartjs-example1").getContext("2d");
                window.myBar = new Chart(ctx, {
                    type: 'line',
                    data: barChartData,
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Orders'
                        }
                    }
                });



            }

            function OnErrorCall(repo) {
                alert("Woops something went wrong, pls try later !");
            }


        });




       

       
       



    </script>
</body>
</html>
